from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include 
from django.contrib import admin


from . import apis

router = DefaultRouter()
router.register('user', apis.UserAPI)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^register/$', apis.UserCreateAPI.as_view(), name='user-create'),
    url(r'^login/$', apis.LoginView.as_view(), name='login'),
    url(r'^logout/$', apis.LogoutView.as_view(), name='logout'),
    url("admin/", admin.site.urls),
    # path for user urls,
    # path for sales urls,
    
]