from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import AbstractBaseUser
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField
from django.contrib.auth.models import BaseUserManager
from django.db import models


class UserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as unique identifiers for authorization
    instead of usernames. The default that's used is "UserManager"
    """

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password,
                                 **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True, null=True, blank=True)
    first_name = models.CharField(null=True, blank=True, max_length=60)
    last_name = models.CharField(null=True, blank=True, max_length=60)
    username = models.CharField(null=True, blank=True, max_length=60)
    city = models.CharField(null=True, blank=True, max_length=100)
    country = CountryField(blank_label='(select country)', multiple=False, null=True, blank=True)
    age = models.PositiveIntegerField(null=True, blank=True)
    USERNAME_FIELD = 'email'
    objects = UserManager()
