from django_countries.serializer_fields import CountryField
from django_countries.serializers import CountryFieldMixin
from .models import User
from rest_framework import serializers, exceptions
import django.contrib.auth.password_validation as validators


class UserCreateSerialize(CountryFieldMixin, serializers.ModelSerializer):
    username = serializers.CharField(required=True, allow_null=False)
    email = serializers.EmailField(required=True, allow_null=False)
    password = serializers.CharField(write_only=True, min_length=6, style={
        'input_type': 'password'}, validators=[validators.validate_password])
    country = CountryField(required=True, allow_null=False)
    city = serializers.CharField(required=True, allow_null=False)
    age = serializers.IntegerField(required=True, allow_null=False)
    first_name = serializers.CharField(required=True, allow_null=False)
    last_name = serializers.CharField(required=True, allow_null=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password', 'country', 'city', 'age')


class UserSerializer(CountryFieldMixin, serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'username', 'password', 'country', 'city', 'age')


